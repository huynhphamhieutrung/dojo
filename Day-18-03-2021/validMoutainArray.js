/**
 * @param {number[]} arr
 * @return {boolean}
 */
var validMountainArray = function(arr) {
  let result = true;
  const max = arr.reduce((previous,current)=>{
    previous < current ? previous = current : previous;

    return previous;
  },0);
  const indexMax = arr.indexOf(max);

  if(arr.length < 3 || arr.length - 1 === indexMax || indexMax === 0){
    result = false;
  }

  for(let i=0; i < indexMax - 1; i++){
    if(arr[i] >= max || arr[i] >= arr[i+1] ) {
      result = false;
    }
  }


  for(let i=indexMax + 1; i < arr.length; i++){
    if(arr[i] >= max || arr[i+1] >= arr[i] ) {
      result = false;
    }
  }

  return result;
};
