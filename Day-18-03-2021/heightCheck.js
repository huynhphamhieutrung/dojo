/**
 * @param {number[]} heights
 * @return {number}
 */
var heightChecker = function(heights) {
  let result = 0;
  let arrPrevious = [];

  for(let i=0; i < heights.length; i++){
    arrPrevious.push(heights[i]);
  }
  heights.sort((a, b) => {
    return a - b;
  });
  for(let i=0; i < heights.length; i++){
    if(heights[i] !== arrPrevious[i]) {
      result ++;
    }
  }

  return result;
};
