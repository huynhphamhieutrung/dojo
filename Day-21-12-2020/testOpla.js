const puppeteer = require('puppeteer');
const USERNAME = "opla@officience.com";
const PASSWORD = "Opla@SuonBiCha";
const NAME = "Opla";
const HAPPINESS = "Opla happiness";
const DAY = "20-11-2020";
const ROLE = "Admin";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({width: 1280, height: 720});
  await page.goto('https://opla-test.officience.com/login', { waitUntil: 'networkidle2' });
  await page.type('input[placeholder="Email"]', USERNAME);
  await page.type('input[placeholder="Password"]', PASSWORD);
  await page.click('button[type="submit"]');

  await page.waitForNavigation();
  await page.goto('https://opla-test.officience.com/me/goals/weekly', { waitUntil: 'networkidle2' });
  await page.click('button[class="btn btn-green btn-plus"]');
  await page.click('a[href="/goals/new"]');

  await page.waitForNavigation();
  await page.goto('https://opla-test.officience.com/goals/new', { waitUntil: 'networkidle2' });
  await page.click('img[alt="Developing Vietnam"]');
  await page.click('button[class="btn-step btn-next"]');
  await page.type('input[placeholder="Add yours"]', 'asdasdasd');
  await page.click('button[class="btn-step btn-next"]');
  await page.type('input[class="ng-untouched ng-pristine ng-valid"]', 'sadasdasd');
  await page.type('input[class="ng-untouched ng-pristine ng-valid"]', 'sadasdasd');
  await page.click('button[class="btn-step btn-next"]');
  await page.type('input[name="startDate"]', '22-12-2020');
  await page.type('input[name="deadline"]', '27-12-2020');
  await page.click('div[class="count-date"]');
  await page.click('button[class="btn-step btn-next"]');
  await page.click('button[class="btn btn-submit"]');

  await page.waitForNavigation();
  await page.goto('https://opla-test.officience.com/me/profile', { waitUntil: 'networkidle2' });
  await page.click('i[class="opla-pen icon-edit-profile"]');
  await page.evaluate( () => document.querySelector("input[name=\"name\"]").value = "")
  await page.evaluate( () => document.querySelector("input[placeholder=\"My happiness is...\"]").value = "")
  await page.evaluate( () => document.querySelector("input[name=\"birthday\"]").value = "")
  await page.evaluate( () => document.querySelector("input[placeholder=\"My role is...\"]").value = "")
  await page.type('input[name="name"]', NAME);
  await page.type('input[placeholder="My happiness is..."]', HAPPINESS);
  await page.type('input[name="birthday"]', DAY);
  await page.type('input[placeholder="My role is..."]', ROLE);
  await page.click('button[class="btn-save"]');

  await setTimeout(async ()=> {
    await browser.close();
  },10000);
})();
