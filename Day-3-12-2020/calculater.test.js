const assert = require('assert');
const {add, subtract, multiply, divide, power, squareRoot} = require('./calculater')

describe('Test addNum function()', () => {
  it('should be error if a & b is not number', () => {
    assert.throws(()=>add(),Error,'This is not number' );
    assert.throws(()=>add('a', ),Error,'This is not number' );
    assert.throws(()=>add([],[]), Error,'This is not number');
    assert.throws(()=>add('a','b'), Error,'This is not number');
    assert.throws(()=>add(undefined,5), Error,'This is not number');
    assert.throws(()=>add(true,5.5), Error, 'This is not number');
    assert.throws(()=>add(1.2,false), Error, 'This is not number');
  });
  it('should be true if a & b is number', () => {
    assert.strictEqual(add(1,1), 2);
    assert.strictEqual(add(2.5,1.5),  4);
    assert.strictEqual(add(10,5),  15);
    assert.strictEqual(add(10.5,5.5), 16);
    assert.strictEqual(add(1.2,2.3),  3.5);
  });
});

describe('Test subtract function()', () => {
  it('should be error if a & b is not number', () => {
    assert.throws(()=>subtract(),Error,'This is not number' );
    assert.throws(()=>subtract('a', ),Error,'This is not number' );
    assert.throws(()=>subtract([],[]), Error,'This is not number');
    assert.throws(()=>subtract('a','b'), Error,'This is not number');
    assert.throws(()=>subtract(undefined,5), Error,'This is not number');
    assert.throws(()=>subtract(true,5.5), Error,'This is not number');
    assert.throws(()=>subtract(1.2,false), Error,'This is not number');
  });
  it('should be true if a & b is number', () => {
    assert.strictEqual(subtract(3,1), 2);
    assert.strictEqual(subtract(5.5,1.5),  4);
    assert.strictEqual(subtract(20,5),  15);
    assert.strictEqual(subtract(21.5,5.5), 16);
    assert.strictEqual(subtract(5.8,2.3),  3.5);
  });
});

describe('Test multiply function()', () => {
  it('should be error if a & b is not number', () => {
    assert.throws(()=>multiply(),Error,'This is not number' );
    assert.throws(()=>multiply('a', ),Error,'This is not number' );
    assert.throws(()=>multiply([],[]), Error,'This is not number');
    assert.throws(()=>multiply('a','b'), Error,'This is not number');
    assert.throws(()=>multiply(undefined,5), Error,'This is not number');
    assert.throws(()=>multiply(true,5.5), Error,'This is not number');
    assert.throws(()=>multiply(1.2,false), Error,'This is not number');
  });
  it('should be true if a & b is number', () => {
    assert.strictEqual(multiply(1,2), 2);
    assert.strictEqual(multiply(2.5,1.5),  3.75);
    assert.strictEqual(multiply(10,5),  50);
    assert.strictEqual(multiply(10.5,5.5), 57.75);
    assert.strictEqual(multiply(1.2,2.3),  2.76);
  });
});


describe('Test addNum function()', () => {
  it('should be error if a & b is not number', () => {
    assert.throws(()=>divide(),Error,'This is not number' );
    assert.throws(()=>divide('a', ),Error,'This is not number' );;
    assert.throws(()=>divide([],[]), Error,'This is not number');
    assert.throws(()=>divide('a','b'), Error,'This is not number');
    assert.throws(()=>divide(undefined,5), Error,'This is not number');
    assert.throws(()=>divide(true,5.5), Error,'This is not number');
    assert.throws(()=>divide(1.2,false), Error,'This is not number');
  });
  it('should be true if a & b is number', () => {
    assert.strictEqual(divide(2,1), 2);
    assert.strictEqual(divide(10,5),  2);
    assert.strictEqual(divide(15,5), 3);
  });
});

describe('Test addNum function()', () => {
  it('should be error if a & b is not number', () => {
    assert.throws(()=>power(),Error,'This is not number' );
    assert.throws(()=>power('a', ),Error,'This is not number' );
    assert.throws(()=>power([],[]), Error,'This is not number');
    assert.throws(()=>power('a','b'), Error,'This is not number');
    assert.throws(()=>power(undefined,5), Error,'This is not number');
    assert.throws(()=>power(true,5.5), Error,'This is not number');
    assert.throws(()=>power(1.2,false), Error,'This is not number');
  });
  it('should be true if a & b is number', () => {
    assert.strictEqual(power(1,1), 1);
    assert.strictEqual(power(2,2),  4);
    assert.strictEqual(power(3,3),  27);
  });
});

describe('Test squareRoot function()', () => {
  it('should be error if a & b is not number', () => {
    assert.throws(()=>squareRoot(),Error,'This is not number' );
    assert.throws(()=>squareRoot('a', ),Error,'This is not number' );
    assert.throws(()=>squareRoot([]), Error,'This is not number');
    assert.throws(()=>squareRoot(undefined), Error,'This is not number');
    assert.throws(()=>squareRoot(true), Error,'This is not number');
  });
  it('should be true if a & b is number', () => {
    assert.strictEqual(squareRoot(1), 1);
    assert.strictEqual(squareRoot(4), 2);
    assert.strictEqual(squareRoot(16),  4);
  });
});

