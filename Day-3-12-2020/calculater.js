function checkNumber(a, b) {
  if (!Number.isFinite(a) || !Number.isFinite(b)) {
    throw new Error('This is not number');
  }
}

const add = (a, b) => {
  checkNumber(a, b);
  return a + b;
};

const subtract = (a, b) => {
  checkNumber(a, b);
  return a - b;
};

const multiply = (a, b) => {
  checkNumber(a, b);
  return a * b;
};

const divide = (a, b) => {
  checkNumber(a, b);
  return a / b;
};

const power = (a, b) => {
  checkNumber(a, b);
  return Math.pow(a,b);
};

const squareRoot = (a) => {
  if (!Number.isFinite(a)) {
    throw new Error('This is not number');
  }
  return Math.sqrt(a);
};

module.exports = {add, subtract, multiply, divide, power, squareRoot};
