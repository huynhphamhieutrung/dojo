function vietlotEzGame() {
  const raNhieu = [19, 12, 53, 52, 22, 20, 04, 02, 54, 18];
  const raIt = [17, 42, 37, 40, 26, 25, 39, 46, 27, 23];
  const datBiet = [52, 50, 19, 14, 22, 12, 04, 34, 45, 20];
  const mangDefault = [];
  const result = [];

  for (let i=1; i <56; i++) {
    mangDefault.push(i);
  }
  const mang = mangDefault.concat(raNhieu).concat(datBiet).concat(raIt);

  for (let i = 0; i < 6; i++) {
    result.push(mang[Math.floor(Math.random() * mang.length)]);
  }

  let kq = Array.from(new Set(result));

  if(6 !== kq.length) {
    return kq = vietlotEzGame();
  }

  const checkNhieu = raNhieu.reduce((value,item) =>{
    kq.includes((item)) ? value += 1 : value;
    return value;
  }, 0)

  const checkIt = raIt.reduce((value,item) =>{
    kq.includes((item)) ? value += 1 : value;
    return value;
  }, 0)

  if(checkNhieu >= 2 || checkIt >= 2) {
    kq = vietlotEzGame();
  } else {
    console.log(kq);
    return kq;
  }
}

vietlotEzGame();
