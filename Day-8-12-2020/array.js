function array() {
  const anh = {
    name: 'trung',
    age: 22,
    phone: 010212,
  };
  const em = {
    name: 'trang',
    address: '134 ABC',
  };
  const em1 = {
    name: 'trang1',
    like: 'play chess',
  };

  const arr = [];
  arr.push(anh, em, em1);

  let sumName = arr.reduce((value, item) => {
    return value.concat(Object.getOwnPropertyNames(item));
  }, []);
  const resultName = [...new Set(sumName)];
  const result = [];
  resultName.forEach((value)=> {
    result[value] = [];
  })
  let resultValueName = Object.assign({}, result);

  resultName.forEach((valueName)=>{
    arr.forEach((value) => {
      resultValueName[valueName].push(value[valueName] || '');
    })
  })
  return resultValueName;
}
const a = array();
console.log(a);

// TODO:cac ham delete su ly voi array
// const a = ['a','b','c','d','e','f'];
//Delete
// a.splice(2,1);
// a[2]='';
// delete a[2];
// const b = a.filter((value)=> a.indexOf(value) !== 2);

//Add
// a.push('g');

//Edit
// a[0] = 'trung';
// a.splice(0,1,'ni','deptrai');
// const b = a.slice(0,2)
